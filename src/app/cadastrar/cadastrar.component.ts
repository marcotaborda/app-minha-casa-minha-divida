﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService, PessoaService, UsuarioService, AuthenticationService } from '@/_services';
import { Pessoa, Usuario } from '@/_models';

@Component({ templateUrl: 'cadastrar.component.html' })
export class CadastrarComponent implements OnInit {
    formCadastro: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private pessoaService: PessoaService,
        private usuarioService: UsuarioService,
        private alertService: AlertService,
    ) {
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.formCadastro = this.formBuilder.group({
            nome: ['', Validators.required],
            email: ['', Validators.required],
            username: ['', Validators.required],
            senha: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    get f() { return this.formCadastro.controls; }

    onSubmit() {
        this.submitted = true;
        this.alertService.clear();

        if (this.formCadastro.invalid) {
            return;
        }

        this.loading = true;

        let dadosCadastro = this.formCadastro.value;

        let pessoaCadastro = new Pessoa;
        pessoaCadastro.nome = dadosCadastro.nome;
        pessoaCadastro.email = dadosCadastro.email;

        let usuarioParams : any;

        this.pessoaService.cadastrar(pessoaCadastro, null)
            .pipe()
            .subscribe(pessoa => {
                pessoaCadastro = pessoa as Pessoa;
                let usuarioCadastro = new Usuario;

                usuarioCadastro.username = dadosCadastro.username;
                usuarioCadastro.senha = dadosCadastro.senha;

                usuarioParams = usuarioCadastro as any;
                usuarioParams.pessoa = {
                    id_pessoa: pessoaCadastro.id_pessoa
                }

                this.usuarioService.cadastrar(usuarioParams)
            .pipe()
            .subscribe(
                data => {
                    console.log(data)
                    this.alertService.success('Cadastro realizado com sucesso.', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
            });
    }
}
