import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, CorretorService, EnderecoService, PessoaService } from '@/_services';
import { Endereco, Pessoa } from '@/_models';

@Component({ templateUrl: 'novocorretor.component.html' })
export class NovoCorretorComponent implements OnInit {
    formCadastro: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private enderecoService: EnderecoService,
        private pessoaService: PessoaService,
        private corretorService: CorretorService
    ) { }

    ngOnInit() {
        this.formCadastro = this.formBuilder.group({
            nome: ['', Validators.required],
            email: ['', Validators.required],
            telefone: ['', Validators.nullValidator],
            endereco: ['', Validators.required],
            numero: ['', Validators.required],
            bairro: ['', Validators.required],
            cidade: ['', Validators.required],
            pais: ['', Validators.required]
        });
    }

    get f() { return this.formCadastro.controls; }

    onSubmit() {
        this.submitted = true;
        this.alertService.clear();

        if (this.formCadastro.invalid) {
            return;
        }

        this.loading = true;

        let corretor = this.formCadastro.value;

        let enderecoCadastro = new Endereco;
        enderecoCadastro.endereco = corretor.endereco;
        enderecoCadastro.numero = corretor.numero;
        enderecoCadastro.bairro = corretor.bairro;
        enderecoCadastro.cidade = corretor.cidade;
        enderecoCadastro.pais = corretor.pais;

        this.enderecoService.cadastrar(enderecoCadastro)
            .pipe()
            .subscribe(endereco => {
                enderecoCadastro = endereco as Endereco;

                let pessoaCadastro = new Pessoa;
                pessoaCadastro.nome = corretor.nome;
                pessoaCadastro.email = corretor.email;
                pessoaCadastro.telefone = corretor.telefone;

                this.pessoaService.cadastrar(pessoaCadastro, enderecoCadastro.id_endereco)
                    .pipe()
                    .subscribe(pessoa => {
                        pessoaCadastro = pessoa as Pessoa;
                        this.corretorService.cadastrar({pessoa: { id_pessoa: pessoaCadastro.id_pessoa }})
                            .pipe(first())
                            .subscribe(
                                data => {
                                    this.alertService.success('Corretor cadastrado com sucesso.', true);
                                    this.router.navigate(['/corretores']);
                                },
                                error => {
                                    this.alertService.error(error);
                                    this.loading = false;
                                });
                    });
                });
    }
}
