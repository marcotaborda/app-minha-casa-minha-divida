﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { ImovelService, AuthenticationService } from '@/_services';
import { Usuario } from '@/_models';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {
    usuarioSessao: Usuario;
    imoveis = [];

    constructor(
        private authenticationService: AuthenticationService,
        private imovelService: ImovelService
    ) { }

    ngOnInit() {
        this.usuarioSessao = this.authenticationService.currentUserValue
        this.listarImoveis();
    }

    excluirImovel(id: number) {
        this.imovelService.excluir(id)
            .pipe(first())
            .subscribe(() => this.listarImoveis());
    }

    private listarImoveis() {
        this.imovelService.listar()
            .pipe(first())
            .subscribe(imoveis => this.imoveis = imoveis);
    }
}
