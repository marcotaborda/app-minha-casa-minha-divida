import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

<<<<<<< HEAD
import { Endereco, Imovel, Corretor, Proprietario } from '@/_models';
import { AlertService, ImovelService, ProprietarioService, CorretorService, EnderecoService } from '@/_services';
=======
import { AlertService, AuthenticationService, ImovelService, EnderecoService, PessoaService, UsuarioService, CorretorService, ProprietarioService } from '@/_services';
import { Endereco, Pessoa, Imovel, Corretor, Proprietario } from '@/_models';
>>>>>>> 25da537159335ec467afa9a2ed3eeac01aec4de1

@Component({ templateUrl: 'novoimovel.component.html' })
export class NovoImovelComponent implements OnInit {
    //usuarioSessao: Usuario;
    formCadastro: FormGroup;
    loading = false;
    submitted = false;
    proprietarios = [];
    corretores = [];

    constructor(
        private authenticationService: AuthenticationService,
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private imovelService: ImovelService,
<<<<<<< HEAD
        private proprietarioService: ProprietarioService,
        private corretorService: CorretorService,
        private enderecoService: EnderecoService
=======
        private enderecoService: EnderecoService,
        private pessoaService: PessoaService,
        private corretorService: CorretorService,
        private proprietarioService: ProprietarioService,
        private usuarioService: UsuarioService
>>>>>>> 25da537159335ec467afa9a2ed3eeac01aec4de1
    ) { }

    ngOnInit() {
        //this.usuarioSessao = this.authenticationService.currentUserValue
        this.formCadastro = this.formBuilder.group({
            endereco: ['', Validators.required],
            corretor: ['', Validators.required],
            proprietario: ['', Validators.required],
            financiado: ['', Validators.required],
            pertence_proprietario: ['', Validators.required],
            aceita_negociacao: ['', Validators.required],
            // valor: ['', Validators.required],
            // imagem: ['', Validators.required],
            // data_cadastro: ['', Validators.required],
            // data_atualizacao: ['', Validators.required],
            // alarme_ultima_atualizacao: ['', Validators.required]
        });

        this.listarProprietarios();
        this.listarCorretores();
    }


    changeFile(input) {
      let file = input.target.files[0];
      let reader = new FileReader();
      reader.readAsText(file);
      this.formCadastro.value.imagem = reader;
    }

    get f() { return this.formCadastro.controls; }

    onSubmit() {
        this.submitted = true;
        this.alertService.clear();

        if (this.formCadastro.invalid) {
            return;
        }

        this.loading = true;

        let imovel = this.formCadastro.value;

        let enderecoCadastro = new Endereco;
        enderecoCadastro.endereco = imovel.endereco;
        enderecoCadastro.numero = imovel.numero;
        enderecoCadastro.bairro = imovel.bairro;
        enderecoCadastro.cidade = imovel.cidade;
        enderecoCadastro.pais = imovel.pais;

        this.enderecoService.cadastrar(enderecoCadastro)
            .pipe()
            .subscribe(endereco => {
                enderecoCadastro = endereco as Endereco;

                let proprietarioCadastro = new Proprietario;

                proprietarioCadastro.pessoa.nome = "fasdfsd";//this.authenticationService.currentUserValue;

                this.proprietarioService.cadastrar(proprietarioCadastro)
                    .pipe()
                    .subscribe(proprietario => {
                        proprietarioCadastro = proprietario as Proprietario;

                        let imovelCadastro = new Imovel;
                        imovelCadastro.financiado = imovel.financiado;
                        imovelCadastro.pertence_proprietario = imovel.pertence_proprietario;
                        imovelCadastro.aceita_negociacao = imovel.aceita_negociacao;
                        imovelCadastro.valor = imovel.valor;
                        imovelCadastro.imagem = imovel.imagem;
                        imovelCadastro.data_cadastro = imovel.data_cadastro;
                        imovelCadastro.data_atualizacao = imovel.data_atualizacao;
                        imovelCadastro.alarme_ultima_atualizacao = imovel.alarme_ultima_atualizacao;

                        this.imovelService.cadastrar(imovelCadastro)
                            .pipe()
                            .subscribe(imovel => {
                                imovelCadastro = imovel as Imovel;
                                this.corretorService.cadastrar({imovel: { id_imovel: imovelCadastro.id_imovel }})
                                    .pipe(first())
                                    .subscribe(
                                        data => {
                                            this.alertService.success('Imóvel cadastrado com sucesso.', true);
                                            this.router.navigate(['/imoveis']);
                                        },
                                        error => {
                                            this.alertService.error(error);
                                            this.loading = false;
                                        });
                            });
                        });
              });

      }
}
