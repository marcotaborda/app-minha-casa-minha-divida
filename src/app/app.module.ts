﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { appRoutingModule } from './app.routing';
import { JwtInterceptor } from './_helpers';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { CadastrarComponent } from './cadastrar';
import { AlertComponent } from './_components';
import { ImovelComponent } from './imovel';
import { ProprietarioComponent } from './proprietario';
import { NovoProprietarioComponent } from './novoproprietario/novoproprietario.component';
import { CorretorComponent } from './corretor';
import { NovoCorretorComponent } from './novocorretor';
import { NovoImovelComponent } from './novoimovel';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        ImovelComponent,
        NovoImovelComponent,
        ProprietarioComponent,
        NovoProprietarioComponent,
        CorretorComponent,
        NovoCorretorComponent,
        CadastrarComponent,
        AlertComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };