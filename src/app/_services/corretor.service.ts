import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Corretor, Endereco, Pessoa } from '@/_models';
import { EnderecoService } from './endereco.service';
import { PessoaService } from './pessoa.service';

@Injectable({ providedIn: 'root' })
export class CorretorService {
    constructor(private http: HttpClient,
        private enderecoService: EnderecoService,
        private pessoaService: PessoaService) { }

    listar() {
        return this.http.get<Corretor[]>(`${config.apiUrl}/corretores/listar`);
    }

    cadastrar(corretor: any) {
        return this.http.post(`${config.apiUrl}/corretores/cadastrar`, corretor);
    }

    excluir(id: number) {
        return this.http.request('DELETE', `${config.apiUrl}/corretores/excluir`, {
            body: {
                id_corretor: id
            }
        });
    }
}