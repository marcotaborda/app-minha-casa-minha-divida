import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Endereco } from '@/_models';

@Injectable({ providedIn: 'root' })
export class EnderecoService {
    constructor(private http: HttpClient) { }

    listar() {
        return this.http.get<Endereco[]>(`${config.apiUrl}/enderecos/listar`);
    }

    cadastrar(endereco: Endereco) {
        return this.http.post(`${config.apiUrl}/enderecos/cadastrar`, endereco);
    }
}