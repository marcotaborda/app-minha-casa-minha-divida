import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pessoa } from '@/_models';

@Injectable({ providedIn: 'root' })
export class PessoaService {
    constructor(private http: HttpClient) { }

    cadastrar(pessoa: Pessoa, id_endereco: number) {
        let pessoaParams = pessoa as any;
        if (id_endereco !== null) {
            pessoaParams.endereco = {
                id_endereco
            }
        }
        return this.http.post(`${config.apiUrl}/pessoas/cadastrar`, pessoaParams);
    }
}