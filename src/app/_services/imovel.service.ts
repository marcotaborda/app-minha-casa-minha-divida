import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Imovel } from '@/_models';

@Injectable({ providedIn: 'root' })
export class ImovelService {
    constructor(private http: HttpClient) { }

    listar() {
        return this.http.get<Imovel[]>(`${config.apiUrl}/imoveis/listar`);
    }

    filtrar(endereco: string, alarme: boolean) {
        return this.http.get<Imovel[]>(`${config.apiUrl}/imoveis/listar?endereco=${endereco}&&alarme=${alarme}`);
    }

    cadastrar(imovel: Imovel, id_endereco: number) {
      let imovelParams = imovel as any;
      if (id_endereco !== null) {
          imovelParams.endereco = {
              id_endereco
          }
      }
      return this.http.post(`${config.apiUrl}/imoveis/cadastrar`, imovelParams);
    }

    excluir(id: number) {
        return this.http.delete(`${config.apiUrl}/imoveis/excluir/${id}`);
    }
}
