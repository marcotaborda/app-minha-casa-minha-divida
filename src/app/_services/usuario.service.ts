import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Usuario, Pessoa } from '@/_models';
import { PessoaService } from './pessoa.service';

@Injectable({ providedIn: 'root' })
export class UsuarioService {
    constructor(private http: HttpClient, private pessoaService: PessoaService) { }

    listar() {
        return this.http.get<Usuario[]>(`${config.apiUrl}/usuarios/listar`);
    }

    cadastrar(usuario: any) {
        return this.http.post(`${config.apiUrl}/usuarios/cadastrar`, usuario);
    }

    excluir(id: number) {
        return this.http.delete(`${config.apiUrl}/usuarios/excluir/${id}`);
    }
}