import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Proprietario, Endereco, Pessoa } from '@/_models';
import { PessoaService } from './pessoa.service';
import { EnderecoService } from './endereco.service';

@Injectable({ providedIn: 'root' })
export class ProprietarioService {;

    constructor(private http: HttpClient, 
        private pessoaService: PessoaService,
        private enderecoService: EnderecoService) {
    }

    listar() {
        return this.http.get<Proprietario[]>(`${config.apiUrl}/proprietarios/listar`);
    }

    cadastrar(proprietario: any) {
        return this.http.post(`${config.apiUrl}/proprietarios/cadastrar`, proprietario);
    }

    excluir(id: number) {
        return this.http.request('DELETE', `${config.apiUrl}/proprietarios/excluir`, {
            body: {
                id_proprietario: id
            }
        });
    }
}