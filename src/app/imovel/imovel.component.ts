﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { ImovelService } from '@/_services';
import { DomSanitizer } from '@angular/platform-browser';

@Component({ templateUrl: 'imovel.component.html' })
export class ImovelComponent implements OnInit {
    imagem: any;
    imoveis = [];

    constructor(
        private sanitizer: DomSanitizer,
        private imovelService: ImovelService
    ) { }

    ngOnInit() {
        this.listarImoveis();
    }

    excluirImovel(id: number) {
        this.imovelService.excluir(id)
            .pipe(first())
            .subscribe(() => this.listarImoveis());
    }

    exibirImagem(blob: Blob) {
        return this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + blob);
    }

    private listarImoveis() {
        this.imovelService.listar()
            .pipe(first())
            .subscribe(imoveis => this.imoveis = imoveis);
    }
}
