import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, ProprietarioService, EnderecoService, PessoaService } from '@/_services';
import { Endereco, Pessoa } from '@/_models';

@Component({ templateUrl: 'novoproprietario.component.html' })
export class NovoProprietarioComponent implements OnInit {
    formCadastro: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private enderecoService: EnderecoService,
        private pessoaService: PessoaService,
        private proprietarioService: ProprietarioService
    ) { }

    ngOnInit() {
        this.formCadastro = this.formBuilder.group({
            nome: ['', Validators.required],
            email: ['', Validators.required],
            telefone: ['', Validators.nullValidator],
            endereco: ['', Validators.required],
            numero: ['', Validators.required],
            bairro: ['', Validators.required],
            cidade: ['', Validators.required],
            pais: ['', Validators.required]
        });
    }

    get f() { return this.formCadastro.controls; }

    onSubmit() {
        this.submitted = true;
        this.alertService.clear();

        if (this.formCadastro.invalid) {
            return;
        }

        this.loading = true;

        let proprietario = this.formCadastro.value;

        let enderecoCadastro = new Endereco;
        enderecoCadastro.endereco = proprietario.endereco;
        enderecoCadastro.numero = proprietario.numero;
        enderecoCadastro.bairro = proprietario.bairro;
        enderecoCadastro.cidade = proprietario.cidade;
        enderecoCadastro.pais = proprietario.pais;

        this.enderecoService.cadastrar(enderecoCadastro)
            .pipe()
            .subscribe(endereco => {
                enderecoCadastro = endereco as Endereco;

                let pessoaCadastro = new Pessoa;
                pessoaCadastro.nome = proprietario.nome;
                pessoaCadastro.email = proprietario.email;
                pessoaCadastro.telefone = proprietario.telefone;

                this.pessoaService.cadastrar(pessoaCadastro, enderecoCadastro.id_endereco)
                    .pipe()
                    .subscribe(pessoa => {
                        pessoaCadastro = pessoa as Pessoa;
                        this.proprietarioService.cadastrar({pessoa: { id_pessoa: pessoaCadastro.id_pessoa }})
                            .pipe(first())
                            .subscribe(
                                data => {
                                    this.alertService.success('Proprietário cadastrado com sucesso.', true);
                                    this.router.navigate(['/proprietarios']);
                                },
                                error => {
                                    this.alertService.error(error);
                                    this.loading = false;
                                });
                    });
                });
    }
}
