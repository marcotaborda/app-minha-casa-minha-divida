﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { ProprietarioService } from '@/_services';

@Component({ templateUrl: 'proprietario.component.html' })
export class ProprietarioComponent implements OnInit {
    proprietarios = [];

    constructor(
        private proprietarioService: ProprietarioService
    ) { }

    ngOnInit() {
        this.listarProprietarios();
    }

    excluirProprietario(id: number) {
        this.proprietarioService.excluir(id)
            .pipe(first())
            .subscribe(() => this.listarProprietarios());
    }

    private listarProprietarios() {
        this.proprietarioService.listar()
            .pipe(first())
            .subscribe(proprietarios => this.proprietarios = proprietarios);
    }
}