﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { CadastrarComponent } from './cadastrar';
import { AuthGuard } from './_helpers';
import { ImovelComponent } from './imovel';
import { ProprietarioComponent } from './proprietario';
import { NovoProprietarioComponent } from './novoproprietario';
import { NovoCorretorComponent } from './novocorretor';
import { CorretorComponent } from './corretor';
import { NovoImovelComponent } from './novoimovel';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'imoveis', component: ImovelComponent },
    { path: 'cadastrarImovel', component: NovoImovelComponent},
    { path: 'proprietarios', component: ProprietarioComponent},
    { path: 'cadastrarProprietario', component: NovoProprietarioComponent},
    { path: 'corretores', component: CorretorComponent},
    { path: 'cadastrarCorretor', component: NovoCorretorComponent},
    { path: 'cadastrar', component: CadastrarComponent },
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);