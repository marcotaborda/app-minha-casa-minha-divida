import { Pessoa } from "./pessoa";

export class Proprietario {
    id_proprietario: number;
    pessoa: Pessoa|null;
}