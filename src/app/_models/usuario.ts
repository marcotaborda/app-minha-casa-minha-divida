﻿import { Pessoa } from "./pessoa";

export class Usuario {
    id_usuario: number;
    pessoa: Pessoa|null;
    username: string;
    senha: string;
    data_acesso: Date;
    token: string;
}