export class Endereco {
    id_endereco: number;
    endereco: string;
    numero: string;
    bairro: string;
    cidade: string;
    pais: string;
}