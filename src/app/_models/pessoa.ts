import { Endereco } from "./endereco";

export class Pessoa {
    id_pessoa: number;
    endereco: Endereco|null;
    nome: string;
    email: string;
    telefone: string;
}