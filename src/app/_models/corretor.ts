import { Pessoa } from "./pessoa";

export class Corretor {
    id_corretor: number;
    pessoa: Pessoa|null;
}