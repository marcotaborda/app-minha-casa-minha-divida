import { Endereco } from "./endereco";
import { Corretor } from "./corretor";
import { Proprietario } from "./proprietario";
import { Usuario } from "./usuario";

export class Imovel {
    id_imovel: number;
    endereco: Endereco|null;
    corretor: Corretor|null;
    proprietario: Proprietario|null;
    financiado: boolean;
    pertence_proprietario: boolean;
    aceita_negociacao: boolean;
    valor: DoubleRange;
    imagem: FileReader;
    data_cadastro: Date;
    data_atualizacao: Date;
    alarme_ultima_atualizacao: boolean;
    usuario: Usuario|null;
}
