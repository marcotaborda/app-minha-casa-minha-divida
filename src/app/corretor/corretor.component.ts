﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { CorretorService } from '@/_services';

@Component({ templateUrl: 'corretor.component.html' })
export class CorretorComponent implements OnInit {
    corretores = [];

    constructor(
        private corretorService: CorretorService
    ) { }

    ngOnInit() {
        this.listarCorretores();
    }

    excluirCorretor(id: number) {
        this.corretorService.excluir(id)
            .pipe(first())
            .subscribe(() => this.listarCorretores());
    }

    private listarCorretores() {
        this.corretorService.listar()
            .pipe(first())
            .subscribe(corretores => this.corretores = corretores);
    }
}